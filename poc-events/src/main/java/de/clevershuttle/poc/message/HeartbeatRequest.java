package de.clevershuttle.poc.message;

import java.io.Serializable;

public class HeartbeatRequest implements Serializable {

	private long systemMillis;
	private String sourceNode;

	public HeartbeatRequest(String sourceNode) {
		this.sourceNode = sourceNode;
		this.systemMillis = System.currentTimeMillis();
	}

	public long getSystemMillis() {
		return systemMillis;
	}

	public String getSourceNode() {
		return sourceNode;
	}
}
