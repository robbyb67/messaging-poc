package de.clevershuttle.poc.message;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountingDebt implements Serializable {
	private String customerId;
	private BigDecimal amount;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "AccountingDebt(" + this.getCustomerId() + "): " + this.getAmount().toPlainString();
	}
}
