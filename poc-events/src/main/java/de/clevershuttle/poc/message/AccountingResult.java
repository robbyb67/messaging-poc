package de.clevershuttle.poc.message;

import java.io.Serializable;

public class AccountingResult implements Serializable {
	private String customerId;
	private String accountingRefId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountingRefId() {
		return accountingRefId;
	}

	public void setAccountingRefId(String accountingRefId) {
		this.accountingRefId = accountingRefId;
	}

	@Override
	public String toString() {
		return "AccountingResult{" +
				"customerId='" + customerId + '\'' +
				", accountingRefId='" + accountingRefId + '\'' +
				'}';
	}
}
