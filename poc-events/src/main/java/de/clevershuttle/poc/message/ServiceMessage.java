package de.clevershuttle.poc.message;

import java.io.Serializable;
import java.util.UUID;

public class ServiceMessage<K> implements Serializable {
	private String version;
	private String consumerRefId;
	private String accountingRefId;

	private K payload;

	public ServiceMessage() {
		this.version = "0.1";
		this.accountingRefId = null;
		this.consumerRefId = null;
	}

	public ServiceMessage(K payload) {
		this();
		this.payload = payload;
		this.consumerRefId = UUID.randomUUID().toString();
	}

	public ServiceMessage(String consumerRef) {
		this();
		this.consumerRefId = consumerRef;
	}

	public String getConsumerRefId() {
		return consumerRefId;
	}

	public void setConsumerRefId(String consumerRefId) {
		this.consumerRefId = consumerRefId;
	}

	public String getAccountingRefId() {
		return accountingRefId;
	}

	public void setAccountingRefId(String accountingRefId) {
		this.accountingRefId = accountingRefId;
	}

	public K getPayload() {
		return payload;
	}

	public void setPayload(K payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "AmqpMessage{" +
				"version='" + version + '\'' +
				", consumerRefId='" + consumerRefId + '\'' +
				", accountingRefId='" + accountingRefId + '\'' +
				", payload=" + payload +
				'}';
	}
}
