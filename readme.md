# Embedded service messaging

## Summary

Each service starts an embedded Apache Artemis message broker using 
a cluster node configuration.

The cluster uses UDP broadcast and discovery to obtain all available
cluster nodes and builds up an Artemis core bridge to each available node.

As result all JMS messages are available on each node.

## Build

    mvn clean install
    
## Run

### Node 1
    cd activemq-node1
    mvn spring-boot:run
    
### Node 2
    cd activemq-node2
    mvn spring-boot:run
    