package de.clevershuttle.messaging.artemis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.activemq.artemis.api.core.BroadcastGroupConfiguration;
import org.apache.activemq.artemis.api.core.DiscoveryGroupConfiguration;
import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.core.UDPBroadcastEndpointFactory;
import org.apache.activemq.artemis.core.config.ClusterConnectionConfiguration;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyAcceptorFactory;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory;
import org.apache.activemq.artemis.core.remoting.impl.netty.TransportConstants;

public class ArtemisConfigurer {

	private ArtemisConfigurer() {
		super();
	}

	public static void configureAsClusterNode(org.apache.activemq.artemis.core.config.Configuration configuration, ArtemisClusterProperties props) {
		// Configure a connector/acceptor pair for client connections from outside the cluster
		Map<String, Object> clientConnectionParams = new HashMap<>();
		clientConnectionParams.put(TransportConstants.ACTIVEMQ_SERVER_NAME, props.getClientHost());
		clientConnectionParams.put(TransportConstants.PORT_PROP_NAME, props.getClientPort());
		configuration.addConnectorConfiguration(props.getClientConnectorName(), new TransportConfiguration(NettyConnectorFactory.class.getName(), clientConnectionParams));
		configuration.addAcceptorConfiguration(new TransportConfiguration(NettyAcceptorFactory.class.getName(), clientConnectionParams));

		// Configure a connector/acceptor pair for connections between cluster nodes
		Map<String, Object> bridgeConnectionParams = new HashMap<>();
		bridgeConnectionParams.put(TransportConstants.ACTIVEMQ_SERVER_NAME, props.getCoreBridgeHost());
		bridgeConnectionParams.put(TransportConstants.PORT_PROP_NAME, props.getCoreBridgePort());
		TransportConfiguration coreBridgeConnector = new TransportConfiguration(NettyConnectorFactory.class.getName(), bridgeConnectionParams);
		TransportConfiguration coreBridgeAcceptor = new TransportConfiguration(NettyAcceptorFactory.class.getName(), bridgeConnectionParams);
		configuration.addConnectorConfiguration(props.getCoreBridgeConnectorName(), coreBridgeConnector);
		configuration.addAcceptorConfiguration(coreBridgeAcceptor);

		// Configure UDP factory for cluster node connection broadcast and discovery
		UDPBroadcastEndpointFactory endpointFactory = new UDPBroadcastEndpointFactory();
		endpointFactory.setGroupAddress(props.getBroadcastMulticastAddress());
		endpointFactory.setGroupPort(props.getBroadcastMulticastPort());

		// Configure UDP broadcast group for cluster node interconnection
		List<String> connectorInfos = new ArrayList<>();
		connectorInfos.add(props.getCoreBridgeConnectorName());
		BroadcastGroupConfiguration broadcastGroupConfig = new BroadcastGroupConfiguration();
		broadcastGroupConfig.setName(props.getBroadcastGroupName());
		broadcastGroupConfig.setEndpointFactory(endpointFactory);
		broadcastGroupConfig.setConnectorInfos(connectorInfos);
		configuration.addBroadcastGroupConfiguration(broadcastGroupConfig);

		// Configure cluster node interconnection discovery
		DiscoveryGroupConfiguration discoveryGroupConfig = new DiscoveryGroupConfiguration();
		discoveryGroupConfig.setName(props.getDiscoveryGroupName());
		discoveryGroupConfig.setBroadcastEndpointFactory(endpointFactory);
		configuration.addDiscoveryGroupConfiguration(props.getDiscoveryGroupName(), discoveryGroupConfig);

		// Configure cluster node persistence directories
		configuration.setBindingsDirectory(props.getDataDir() + "/bindings");
		configuration.setJournalDirectory(props.getDataDir() + "/journal");
		configuration.setLargeMessagesDirectory(props.getDataDir() + "/largeMessages");
		configuration.setPagingDirectory(props.getDataDir() + "/paging");
		configuration.setCreateBindingsDir(true);
		configuration.setCreateJournalDir(true);

		// Configure the cluster node
		ClusterConnectionConfiguration clusterConfig = new ClusterConnectionConfiguration();
		clusterConfig.setName(props.getClusterName());
		clusterConfig.setDiscoveryGroupName(props.getDiscoveryGroupName());
		clusterConfig.setConnectorName(props.getCoreBridgeConnectorName());
		clusterConfig.setDuplicateDetection(true);
		configuration.addClusterConfiguration(clusterConfig);

	}
}
