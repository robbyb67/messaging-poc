package de.clevershuttle.messaging.artemis;

public interface ArtemisClusterProperties {

	// Properties for non-cluster-node clients connecting to the broker
	String getClientConnectorName();
	String getClientHost();
	int getClientPort();

	// Properties for cluster inter-node connection
	String getCoreBridgeConnectorName();
	String getCoreBridgeHost();
	int getCoreBridgePort();

	// Properties for the cluster node to broadcast it's bridging connectors
	String getBroadcastGroupName();
	String getBroadcastMulticastAddress();
	int getBroadcastMulticastPort();

	// Property for the cluster node where to obtain bridging connectors from
	String getDiscoveryGroupName();

	// Property for the cluster naming and data
	String getClusterName();
	String getDataDir();
}
