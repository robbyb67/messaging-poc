package de.clevershuttle.poc.activemqservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisConfigurationCustomizer;
import org.springframework.context.annotation.Configuration;

import de.clevershuttle.messaging.artemis.ArtemisClusterProperties;
import de.clevershuttle.messaging.artemis.ArtemisConfigurer;

@Configuration
public class ArtemisConfig implements ArtemisConfigurationCustomizer, ArtemisClusterProperties {

	private static final String CLIENT_CONNECTOR_NAME = "clientConnector";
	private static final String CLUSTER_CONNECTOR_NAME = "clusterConnector";

	@Value("${spring.artemis.host}")
	private String clientHost;

	@Value("${spring.artemis.port}")
	private int clientPort;

	@Value("${spring.artemis.cluster.host}")
	private String coreBridgeHost;

	@Value("${spring.artemis.cluster.port}")
	private int coreBridgePort;

	@Value("${spring.artemis.multicastAddress}")
	private String multicastAddress;

	@Value("${spring.artemis.multicastPort}")
	private int multicastPort;

	@Value("${spring.artemis.discovery.group-name}")
	private String discoveryGroupName;

	@Value("${spring.artemis.broadcast.group-name}")
	private String broadcastGroupName;

	@Value("${spring.artemis.cluster.name}")
	private String clusterName;

	@Value("${spring.artemis.cluster.data-dir}")
	private String dataDir;

	@Override
	public void customize(org.apache.activemq.artemis.core.config.Configuration configuration) {
		ArtemisConfigurer.configureAsClusterNode(configuration, this);
	}

	@Override
	public String getClientConnectorName() {
		return CLIENT_CONNECTOR_NAME;
	}

	@Override
	public String getClientHost() {
		return this.clientHost;
	}

	@Override
	public int getClientPort() {
		return this.clientPort;
	}

	@Override
	public String getCoreBridgeConnectorName() {
		return CLUSTER_CONNECTOR_NAME;
	}

	@Override
	public String getCoreBridgeHost() {
		return this.coreBridgeHost;
	}

	@Override
	public int getCoreBridgePort() {
		return this.coreBridgePort;
	}

	@Override
	public String getBroadcastGroupName() {
		return this.broadcastGroupName;
	}

	@Override
	public String getBroadcastMulticastAddress() {
		return this.multicastAddress;
	}

	@Override
	public int getBroadcastMulticastPort() {
		return this.multicastPort;
	}

	@Override
	public String getDiscoveryGroupName() {
		return this.discoveryGroupName;
	}

	@Override
	public String getClusterName() {
		return this.clusterName;
	}

	@Override
	public String getDataDir() {
		return this.dataDir;
	}
}