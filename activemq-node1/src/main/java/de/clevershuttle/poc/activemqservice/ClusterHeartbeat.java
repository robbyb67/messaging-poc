package de.clevershuttle.poc.activemqservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import de.clevershuttle.poc.message.HeartbeatRequest;
import de.clevershuttle.poc.message.ServiceMessage;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ClusterHeartbeat {

	@Autowired
	JmsMessagingTemplate jmsTemplate;

	@Value("${spring.application.name}")
	private String sourceNodeName;

	private HeartbeatThread heartbeat;

	private class HeartbeatThread extends Thread {

		@Override
		public void run() {
			while(true) {
				sendHeartbeat();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					log.warn(e.getLocalizedMessage(), e);
				}
			}
		}
	}

	public void startHeartbeat() {
		if( heartbeat == null ) {
			this.heartbeat = new HeartbeatThread();
			this.heartbeat.start();
		}
	}

	protected void sendHeartbeat() {
		HeartbeatRequest hb = new HeartbeatRequest(this.sourceNodeName);
		this.jmsTemplate.convertAndSend("heartbeat", new ServiceMessage<HeartbeatRequest>(hb));
	}

	@JmsListener(destination = "heartbeat")
	public void processHeartbeat(ServiceMessage<HeartbeatRequest> heartbeat) {
		log.info("Heartbeat(" + heartbeat.getConsumerRefId() + ")from " + heartbeat.getPayload().getSourceNode() + " with "
				+ (System.currentTimeMillis() - heartbeat.getPayload().getSystemMillis()) + " ms latency");
	}
}
