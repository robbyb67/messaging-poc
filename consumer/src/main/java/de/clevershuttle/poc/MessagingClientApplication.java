package de.clevershuttle.poc;

import java.math.BigDecimal;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsMessagingTemplate;

import de.clevershuttle.poc.message.AccountingDebt;
import de.clevershuttle.poc.message.ServiceMessage;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class MessagingClientApplication implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	JmsMessagingTemplate messagingTemplate;

	private class RequestThread extends Thread {

		@Override
		public void run() {
			while(true) {
				ServiceMessage<AccountingDebt> message = new ServiceMessage<>(UUID.randomUUID().toString());
				AccountingDebt debt = new AccountingDebt();
				debt.setCustomerId("customerId");
				debt.setAmount(new BigDecimal("10.50"));
				message.setPayload(debt);

				try {
					ServiceMessage<?> result = messagingTemplate.convertSendAndReceive("accounting", message, ServiceMessage.class);
					log.info(result != null ? result.toString() : "null result");
				} catch(JmsException e) {
					log.error(e.getMessage());
				}
				try {
					Thread.sleep(15000);
				} catch (Exception e) {
					log.warn(e.getMessage(), e);
					break;
				}
			}
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(MessagingClientApplication.class, args);
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
		log.info("client application ready");

		RequestThread t = new RequestThread();
		t.start();
	}
}
