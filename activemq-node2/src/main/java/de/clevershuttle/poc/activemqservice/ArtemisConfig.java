package de.clevershuttle.poc.activemqservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisConfigurationCustomizer;
import org.springframework.context.annotation.Configuration;

import de.clevershuttle.messaging.artemis.ArtemisClusterProperties;
import de.clevershuttle.messaging.artemis.ArtemisConfigurer;
import lombok.Getter;

@Configuration
public class ArtemisConfig implements ArtemisConfigurationCustomizer, ArtemisClusterProperties {

	private static final String CLIENT_CONNECTOR_NAME = "clientConnector";
	private static final String CLUSTER_CONNECTOR_NAME = "clusterConnector";

	@Value("${spring.artemis.host}")
	@Getter
	private String clientHost;

	@Value("${spring.artemis.port}")
	@Getter
	private int clientPort;

	@Value("${spring.artemis.cluster.nodeHost}")
	@Getter
	private String coreBridgeHost;

	@Value("${spring.artemis.cluster.nodePort}")
	@Getter
	private int coreBridgePort;

	@Value("${spring.artemis.cluster.broadcastAddress}")
	@Getter
	private String broadcastMulticastAddress;

	@Value("${spring.artemis.cluster.broadcastPort}")
	@Getter
	private int broadcastMulticastPort;

	@Value("${spring.artemis.cluster.discoveryName}")
	@Getter
	private String discoveryGroupName;

	@Value("${spring.artemis.cluster.broadcastName}")
	@Getter
	private String broadcastGroupName;

	@Value("${spring.artemis.cluster.name}")
	@Getter
	private String clusterName;

	@Value("${spring.artemis.cluster.data-dir}")
	@Getter
	private String dataDir;

	@Override
	public void customize(org.apache.activemq.artemis.core.config.Configuration configuration) {
		ArtemisConfigurer.configureAsClusterNode(configuration, this);
	}

	@Override
	public String getClientConnectorName() {
		return CLIENT_CONNECTOR_NAME;
	}

	@Override
	public String getCoreBridgeConnectorName() {
		return CLUSTER_CONNECTOR_NAME;
	}
}