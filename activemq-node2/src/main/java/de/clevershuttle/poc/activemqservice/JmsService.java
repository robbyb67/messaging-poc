package de.clevershuttle.poc.activemqservice;

import java.util.UUID;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import de.clevershuttle.poc.message.AccountingDebt;
import de.clevershuttle.poc.message.AccountingResult;
import de.clevershuttle.poc.message.ServiceMessage;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JmsService {

	@JmsListener(destination = "accounting")
	public ServiceMessage<?> processAccountingMessage(ServiceMessage<?> content) {
		log.info("accounting: " + content.toString());
		if( content.getPayload() instanceof AccountingDebt) {
			AccountingDebt debt = (AccountingDebt)content.getPayload();
			log.info(debt.toString());

			AccountingResult result = new AccountingResult();
			result.setCustomerId(debt.getCustomerId());
			result.setAccountingRefId(UUID.randomUUID().toString());
			ServiceMessage<AccountingResult> message = new ServiceMessage<>(content.getConsumerRefId());
			message.setAccountingRefId(message.getAccountingRefId());
			message.setPayload(result);
			return message;
		}
		return null;
	}
}
