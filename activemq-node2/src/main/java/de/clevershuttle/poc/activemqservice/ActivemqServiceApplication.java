package de.clevershuttle.poc.activemqservice;

import java.math.BigDecimal;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.jms.core.JmsMessagingTemplate;

import de.clevershuttle.poc.message.AccountingDebt;
import de.clevershuttle.poc.message.ServiceMessage;
import lombok.extern.slf4j.Slf4j;


@SpringBootApplication
@EnableIntegration
@Slf4j
public class ActivemqServiceApplication implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	JmsMessagingTemplate jmsTemplate;
	@Autowired
	ClusterHeartbeat heartbeat;

	public static void main(String[] args) {
		SpringApplication.run(ActivemqServiceApplication.class, args);
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
		log.info(applicationReadyEvent.getSpringApplication().getMainApplicationClass().getName() + " ready");

		//this.sendMessage();
		this.heartbeat.startHeartbeat();
	}

	private void sendMessage() {
		ServiceMessage<AccountingDebt> message = new ServiceMessage<>(UUID.randomUUID().toString());
		AccountingDebt debt = new AccountingDebt();
		debt.setCustomerId("customerId");
		debt.setAmount(new BigDecimal("10.50"));
		message.setPayload(debt);

		ServiceMessage<?> result = this.jmsTemplate.convertSendAndReceive("accounting", message, ServiceMessage.class);
		log.info(result != null ? result.toString() : "null result");
	}


}
